import React, {Component} from 'react'
import {connect} from 'react-redux'
import Octokit from "@octokit/rest"
import RepoList from "./components/repolist/RepoList"
import UserList from "./components/userlist/UserList"
import { RingLoader } from 'react-spinners'

// import 'Octokit';
import logo from './logo.svg';
import './App.css';

class App extends Component {

    constructor(){
        super();
        this.state = {
            repos: null,
            users: null,
            userSelected: false,
            currentSearch:"jeresig",
            currentUser:"",
            currentUserPage:1,
            currentRepoPage:1,
            githubResponse: {},
            loading: false
        };

        this.setSearch = this.setSearch.bind(this);
        this.search = this.search.bind(this);
        this.getUsers = this.getUsers.bind(this);

        //instantiate Github REST library instance
        this.github = new Octokit();

        //authorize library using a personal Github token (ideally should use oauth)
        this.github.authenticate({
            type: 'token',
            token: '93961c09b3898ca9f5eaf6b13f9eb3e362cffc8d'
        });
    }

    componentDidMount() {
        this.getUsers(this.state.currentSearch, 1);
    }

    componentWillMount() {

    }

    getUsers = (searchUser, page) => {
        this.setState({loading:true});
        this.github.search.users({
            q: searchUser,
            sort: 'repositories',
            order: 'desc',
            per_page: '20',
            page: page
        }).then(({data}) => {
            // concat new results with current results
            if(this.state.users == null) {
                this.setState({users: data});
            }else{
                data.items = this.state.users.items.concat(data.items);
                this.setState({users: data});
            }
            setTimeout(() => this.setState({loading:false}), 3000);
        })
    }

    getRepos = (login, page) => {
        this.setState({loading:true});
        this.github.search.repos({
            q: 'user:' + login,
            per_page: '20',
            page: page
        }).then(({data}) => {
            // handle data
            if(this.state.repos == null) {
                this.setState({repos: data});
            }else{
                data.items = this.state.repos.items.concat(data.items);
                this.setState({repos: data});
            }
            setTimeout(() => this.setState({loading:false}), 3000);
        })
    }

    //callback function for handling get user event (note: purposely not using Flux / Redux
    handleUser = (user) => {
        this.getRepos(user, 1);
        this.setState({userSelected: true, currentUser: user});
    }

    //take user back to user list from repo list
    handleBack = () => {
        this.setState({userSelected: false});
        this.setState({repos: null, currentRepoPage:1});
    }

    handleKeyPress = (e) => {
      if(e.key == 'Enter'){
        this.search();
      }
    }

    //callback from Users list to handle infinite load
    loadMoreUsers = () => {
        let userPage = this.state.currentUserPage+1;
        this.setState({currentUserPage: userPage});
        this.getUsers(this.state.currentSearch, this.state.currentUserPage);
    }

    //callback from Repos list to handle infinite load
    loadMoreRepos = () => {
        this.setState({loading:true});
        let repoPage = this.state.currentRepoPage+1;
        this.setState({currentRepoPage: repoPage});
        this.getRepos(this.state.currentUser, this.state.currentRepoPage);
    }

    //set current search value when input field is updated
    setSearch(e){
        this.setState({currentSearch:e.target.value});
    }

    //get users by clicking search button
    search(){
        this.setState({loading:true});
        this.handleBack();
        this.setState({users: null, currentUserPage: 1});
        this.getUsers(this.state.currentSearch, 1);
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Github Explorer</h1>
                    <label className="search-label">Search for users: </label>
                    <input className="search" type="text" onKeyPress={ this.handleKeyPress } onChange={ this.setSearch } value={ this.state.currentSearch } tabindex="1"/>
                    <button className="search-button" onClick={this.search} tabindex="1">Explore</button>
                    { this.state.userSelected ?  <button className="back-to-home" onClick={() => this.handleBack()} tabindex="1">Home</button> : null }
                </header>
                <div className="loader">
                    <RingLoader
                        color={'#123abc'}
                        loading={this.state.loading}
                    />
                </div>
                { !this.state.userSelected ? <UserList listitems={this.state.users} handleUser={this.handleUser} handleLoading={this.loadMoreUsers}/> : null }
                { this.state.userSelected ? <RepoList listitems={this.state.repos} handleLoading={this.loadMoreRepos}/> : null }
            </div>
        );
    }
}

export default App;
