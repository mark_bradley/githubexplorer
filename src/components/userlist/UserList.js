import React from 'react'
import PropTypes from 'prop-types'
import _debounce from 'lodash/debounce'

class UserList extends React.Component {
    //add listening when component is ready to listen to scrolling
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    //remove listening when component is ready to listen to scrolling
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    //scrolling to end of list loads new results to the end
    handleScroll = _debounce((e) => {
        if(window.scrollY+(0.3 * window.scrollY) > window.innerHeight){
            this.props.handleLoading();
        }
    }, 500);

    //clicking user loads their repo list
    handleClick = (e, item) => {
        this.props.handleUser(item.login);
    }

    handleKeyPress = (e, item) => {
      if(e.key == 'Enter'){
        this.props.handleUser(item.login);
      }
    }

    render() {
        return (
            <div>
                <h2 className="repo-title">Users</h2>
                <ul className="user-list">
                    {this.props.listitems && this.props.listitems.items.map((item, index) => {
                        return (
                            <li className="user-item" onClick={(e) => this.handleClick(e,item)} onKeyPress={ (e) => this.handleKeyPress(e,item) } key={index} tabindex="2" alt={item.login}>
                                <img src={item.avatar_url} width="60" height="60" alt="User avatar"/>
                                <label aria-label={item.login}>{item.login}</label>
                            </li>
                        )
                    })}
                </ul>
            </div>
        );
    }
}

UserList.propTypes = {
    handleLoading: PropTypes.func,
    handleUser: PropTypes.func
};

export default UserList
