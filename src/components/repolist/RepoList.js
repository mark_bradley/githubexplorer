import React from 'react'
import PropTypes from 'prop-types'
import _debounce from 'lodash/debounce'


class RepoList extends React.Component {
    //add scroll listener for loading data at end of page scroll
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    //remove listener if component is removed
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = _debounce((e) => {
        //if scroll position is greater than the current list height load more
        if(window.scrollY+(0.3 * window.scrollY) > window.innerHeight){
            this.props.handleLoading();
        }
    }, 500);

    handleClick = (e, item) => {
        window.open(item.html_url);
    }

    handleKeyPress = (e, item) => {
      if(e.key == 'Enter'){
        window.open(item.html_url);
      }
    }

    render() {
        return (
            <div>
                <h2 className="repo-title"> {this.props.listitems && this.props.listitems.total_count} Git Repos</h2>
                <ul className="repo-list">
                    {this.props.listitems && this.props.listitems.items.map((item, index) => {
                        return (
                            <li className="repo-item" onClick={(e) => this.handleClick(e,item)} onKeyPress={ (e) => this.handleKeyPress(e, item) } key={index} tabindex="2">
                                <label className="repo-title">{item.name}</label>
                                <label>{item.description && (item.description.length > 150) ? item.description.substring(0,150)+"..." : item.description}</label>
                                <label className="repo-details">
                                    <label><span role="img" aria-label="programming language">💻</span>{item.language || "None specified"}</label>
                                    <label><span role="img" aria-label="stars">⭐</span>{item.stargazers_count}</label>
                                    <label><span role="img" aria-label="forks">🍴</span>{item.forks_count}</label>
                                </label>
                            </li>
                        )
                    })}
                    {this.props.listitems && ( (this.props.listitems.items.length+1) % 2 !== 0) ? "" : <li className="repo-item-filler"></li>}
                </ul>
            </div>
        );
    }
}

RepoList.propTypes = {
    handleLoading: PropTypes.func,
        handleRepo: PropTypes.func,
};

export default RepoList
